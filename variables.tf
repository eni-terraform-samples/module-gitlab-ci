variable "length" {
  description = "the number of words the name should be composed of"
  default     = 1
  type        = number
}